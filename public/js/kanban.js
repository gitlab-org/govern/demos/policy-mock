var h = $(document).innerHeight();
var IssueNum = +1;
var date = new Date();
var year = date.getFullYear();
var month = date.getMonth() + 1;
var day = date.getDate();
if (("" + month).length == 1) {
    month = "0" + month;
}
if (("" + day).length == 1) {
    day = "0" + day;
}
var Today = "" + year + month + day;
$('.js-open-issue').click(function () {
    var activeClass = "js_input-active";
    var mask = document.createElement("div");
    document.body.appendChild(mask);
    mask.classList.add(activeClass);
    $('.input_issues').show();
});
$('.js-submit-issues').click(function () {
    var IssueName = $('#issue').val();
    var IssueType = $('#type option:selected').val();
    var AddIssue = $(".TableNum0").html('<li><dl><dt class=' + IssueType + '>' + IssueName + '-' + IssueNum + '<dd>Lorem upsum coloef condesxi downlo</dd><dd class="date">' + Today + '</dd></dt></dl></li>');
    $('.input_issues').hide();
    $('.js_input-active').hide();
    document.getElementById("myForm").reset();
});

function SortTable() {
    $('.cards').each(function (i) {
        $(".TableNum" + i).sortable({
            connectWith: ".cards"
            , opacity: 0.8
            , update: function () {
                CountingIssues();
            }
        });
        $(".TableNum" + i).disableSelection();
    });
}

function CountingIssues() {
    $('.cards').each(function () {
        var CheckNum = $(this).find('li.number-count').size();
        $(this).prev('h3').find('span').html(CheckNum + "&nbsp;");
    });
}

function SetCard() {
    $('.cards').each(function (i) {
        $(this).addClass('TableNum' + i);
        $(this).find('li').addClass("ui-state-default");
        $(this).height(h);
    });
}
$("h3.resolved").click(ontop);

function ontop() {
    $(".trello").toggleClass("resizing");
    $("h3.resolved").toggleClass("resizingh3");
    $("ul.cards.TableNum3.ui-sortable").toggleClass("resizingcollapse1");
}

// Change title tag, per different column
$('[data-toggle="tooltip"]').tooltip();
$(document).ready(function () {
    $('ul.cards.TableNum0 li dd.date').data('tooltip', false).tooltip({
        //        title: 'Dismiss'
        title: 'Issue'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('ul.cards.TableNum1 li dd.date').data('tooltip', false).tooltip({
        //        title: 'Confirmed'
        title: 'Issue'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('ul.cards.TableNum2 li dd.date').data('tooltip', false).tooltip({
        //        title: 'Resolved'
        title: 'Issue'
    });
    $('[data-toggle="tooltip"]').tooltip();
});
// Change dismiss button toggle
$(".third").click(function () {
    $(this).text(function (i, v) {
        return v === 'Dismiss' ? 'Undo dismiss' : 'Dismiss'
    })
});
// toggle create issue text and add issue icon
$("button.viewissue").click(function () {
    $(this).text(function (i, v) {
        return v === 'Create issue' ? 'View issue' : 'Create issue'
    })
    $('ul.cards.TableNum1 li dd.date.added-issue').css("display", "block");
    $('.show-header').css("display", "block");
});

// show toast message (no timer)
//$("#dismissa").click( function() {
//    $(".toast").toggleClass("show");
//} );
// show toast message (w/timer)
$(".dismissa").click(function (e) {
    e.preventDefault(); // Added for the example "Click" link
    $('.toast').css("display", "block");
    setTimeout(function () {
        $('.toast').css("display", "none");
    }, 3000);
});
$(".dismissa2").click(function (e) {
    e.preventDefault(); // Added for the example "Click" link
    $('.toast').css("display", "block");
    setTimeout(function () {
        $('.toast').css("display", "none");
    }, 3000);
});

$(".dismissb").click(function (e) {
    e.preventDefault(); // Added for the example "Click" link
    $('.toast2').css("display", "block");
    setTimeout(function () {
        $('.toast2').css("display", "none");
    }, 3000);
});


// status dropdown
$(".dropdown-menu li a").click(function () {
    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});


// remove dismiss button OnClick - deprecated
//$(".bold-now").click(function (e) {
//    $('button.dismissa').css("display", "none");
//});

// show toast message (w/timer)
$(".change-column").click(function (e) {
    $('.under-review').css("display", "none");
    $('.under-review-new').css("display", "block");
    $('.dropdown-menu>li>a.bold').css("font-weight", "normal");
    $('.dropdown-menu>li>a.bold-now').css("font-weight", "bold");
});
$(".change-column-again").click(function (e) {
    $('.under-review-new').css("display", "none");
    $('.under-review').css("display", "block");
        $('.dropdown-menu>li>a.bold').css("font-weight", "bold");
    $('.dropdown-menu>li>a.bold-now').css("font-weight", "normal");
});
$(".dismissa").click(function (e) {
    $('.under-review-new').css("display", "none");
    $('.under-review').css("display", "none");
});
$(".dismissa2").click(function (e) {
    $('.under-review2').css("display", "none");
});
$(".dismissb").click(function (e) {
    $('.under-review3').css("display", "none");
});

CountingIssues();
SetCard();
SortTable();

// toggle UI view

$("#kanban_click").click(function (e) {
    $('#table_alert_UI').css("display", "none");
    $('#kanban_alert_UI').css("display", "block");
    $('.kanban-btn').css("background", "lightgray");
    $('.table-btn').css("background", "white");
    $('#img_filter_table').attr('src', 'assets/prototype_alerts3.png');
});

$("#table_click").click(function (e) {
    $('#kanban_alert_UI').css("display", "none");
    $('#table_alert_UI').css("display", "table");
        $('.kanban-btn').css("background", "white");
    $('.table-btn').css("background", "lightgray");
     $('#img_filter_table').attr('src', 'assets/prototype_alerts.png');
});

