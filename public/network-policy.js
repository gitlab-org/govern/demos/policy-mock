const PolicyDirectionInbound = "ingress",
  PolicyDirectionOutbound = "egress";

class NetworkPolicy {
  constructor() {
    this._name = "";
    this._description = "";
    this._endpointSelectorLabels = {};
    this._direction = PolicyDirectionInbound;
    this._rule3 = null;
    this._rule4 = null;
  }

  get apiVersion() {
    return "cilium.io/v2";
  }

  get kind() {
    return "CiliumNetworkPolicy";
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  set endpointSelectorLabels(labels) {
    this._endpointSelectorLabels = labels;
  }

  set direction(direction) {
    this._direction = direction;
  }

  set rule3(rule3) {
    this._rule3 = rule3;
  }

  set rule4(rule4) {
    this._rule4 = rule4;
  }

  get spec() {
    const spec = {
      endpointSelector: { matchLabels: this._endpointSelectorLabels }
    };

    if ((!this._rule3 || !this._rule3.spec) && (!this._rule4 || !this._rule4.spec)) return spec;

    spec[this._direction] = [];
    if (this._rule3 && this._rule3.spec)
      spec[this._direction].push(this._rule3.spec);
    if (this._rule4 && this._rule4.spec)
      spec[this._direction].push(this._rule4.spec);

    return spec;
  }

  toYaml() {
    const policy = {
      apiVersion: this.apiVersion,
      kind: this.kind,
      metadata: {
        name: this.name,
        annotations: this.description
      },
      spec: this.spec
    };
    return jsyaml.safeDump(policy, { noArrayIndent: true });
  }
}

class RuleLabel {
  constructor(labels, direction) {
    this._labels = labels;
    this._direction = direction;
  }

  get spec() {
    let spec = {};
    spec[
      this._direction === PolicyDirectionInbound
        ? "fromEndpoints"
        : "toEndpoints"
    ] = [Object.keys(this._labels).length > 0 ? { matchLabels: this._labels } : {}];
    return spec;
  }
}

class RulePort {
  constructor(ports,direction){
    this._ports = ports;
    this._direction = direction;
  }

  get spec() {
    let spec = {};
    spec[
      this._direction === PolicyDirectionInbound
        ? "fromPorts"
        : "toPorts"
    ] = {"ports": (this._ports.length > 0 ? this._ports : {})};
    return spec;
  }
}

class RuleService {
  constructor(service, direction) {
    this._service = service;
    this._direction = direction;
  }

  get spec() {
    if (this._direction === PolicyDirectionInbound) return null;
    if (!Boolean(this._service)) return null;

    return {
      toService: [{ k8sService: { serviceName: this._service } }]
    };
  }
}

class RuleEntity {
  constructor(entities, direction) {
    this._entities = entities;
    this._direction = direction;
  }

  get spec() {
    let spec = {};
    spec[
      this._direction === PolicyDirectionInbound ? "fromEntities" : "toEntities"
    ] = this._entities;
    return spec;
  }
}

class RuleCIDR {
  constructor(cidr, direction) {
    this._cidr = cidr;
    this._direction = direction;
  }

  get spec() {
    if (!Boolean(this._cidr)) return null;

    let spec = {};
    spec[this._direction === PolicyDirectionInbound ? "fromCIDR" : "toCIDR"] = [
      this._cidr
    ];
    return spec;
  }
}
