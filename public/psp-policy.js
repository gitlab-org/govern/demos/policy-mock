class PSPPolicy {
  constructor() {
    this._name = "";
    this._description = "";
    this._privileged = false;
    this._privilegeEscalation = false;
    this._runAsUser = null;
    this._runAsUser = null;
    this._allowedCapabilities = null;
    this._requiredDropCapabilities = null;
    this._defaultAddCapabilities = null;
    this._forbiddenSysctls = null;
    this._allowedUnsafeSysctls = null;
  }

  get apiVersion() {
    return "policy/v1beta1";
  }

  get kind() {
    return "PodSecurityPolicy";
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  set runAsUser(userRule) {
    this._runAsUser = userRule;
  }

  set runAsGroup(groupRule) {
    this._runAsGroup = groupRule;
  }

  set privileged(allowPrivileged) {
    this._privileged = !allowPrivileged;
  }

  set privilegeEscalation(allowEscalation) {
    this._privilegeEscalation = !allowEscalation;
  }

  set allowedCapabilities(capabilities){
    this._allowedCapabilities = capabilities;
  }

  set requiredDropCapabilities(capabilities){
    this._requiredDropCapabilities = capabilities;
  }

  set defaultAddCapabilities(capabilities){
    this._defaultAddCapabilities = capabilities;
  }

  set forbiddenSysctls(sysctls){
    this._forbiddenSysctls = sysctls;
  }

  set allowedUnsafeSysctls(sysctls){
    this._allowedUnsafeSysctls = sysctls;
  }

  get spec() {
    const spec = [];

    if (this._privileged)
      spec.push({ privileged: false });
    if (this._privilegeEscalation)
      spec.push({ allowPrivilegeEscalation: false });
    if (this._runAsUser && this._runAsUser.spec)
      spec.push({ runAsUser: this._runAsUser.spec });
    if (this._runAsGroup && this._runAsGroup.spec)
      spec.push({ supplementalGroups: this._runAsGroup.spec });
    if (this._allowedCapabilities)
      spec.push({ allowedCapabilities: this._allowedCapabilities });
    if (this._requiredDropCapabilities)
      spec.push({ requiredDropCapabilities: this._requiredDropCapabilities });
    if (this._defaultAddCapabilities)
      spec.push({ defaultAddCapabilities: this._defaultAddCapabilities });
    if (this._forbiddenSysctls)
      spec.push({ forbiddenSysctls: this._forbiddenSysctls });
    if (this._allowedUnsafeSysctls)
      spec.push({ allowedUnsafeSysctls: this._allowedUnsafeSysctls });

    return spec;
  }

  toYaml() {
    const policy = {
      apiVersion: this.apiVersion,
      kind: this.kind,
      metadata: {
        name: this.name,
        annotations: this.description
      },
      spec: this.spec
    };
    return jsyaml.safeDump(policy, { noArrayIndent: true });
  }
}

class RuleUserOrGroup {
  constructor(rule, min, max) {
    this._rule = rule;
    this._min = min;
    this._max = max;
  }

  get spec() {
    let spec = { 
      rule: this._rule
    }

    if(this._min && this._max){
      spec['range'] = [{min: this._min, max: this._max}];
    }
    
    return spec;
  }
}
