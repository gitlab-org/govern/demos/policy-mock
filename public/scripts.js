var counter = 1;
const APPENDTO = 1;
const INSERTBEFORE = 2;
const INSERTAFTER = 3;
const capabilities = ["CAP_AUDIT_CONTROL","CAP_AUDIT_READ","CAP_AUDIT_WRITE","CAP_BLOCK_SUSPEND (since Linux 3.5)","CAP_CHOWN","CAP_DAC_OVERRIDE","CAP_DAC_READ_SEARCH","CAP_FOWNER","CAP_FSETID","CAP_IPC_LOCK","CAP_IPC_OWNER","CAP_KILL","CAP_LEASE","CAP_LINUX_IMMUTABLE","CAP_MAC_ADMIN","CAP_MAC_OVERRIDE","CAP_MKNOD","CAP_NET_ADMIN","CAP_NET_BIND_SERVICE","CAP_NET_BROADCAST","CAP_NET_RAW","CAP_SETGID","CAP_SETFCAP","CAP_SETPCAP","CAP_SETUID","CAP_SYS_ADMIN","CAP_SYS_BOOT","CAP_SYS_CHROOT","CAP_SYS_MODULE","CAP_SYS_NICE","CAP_SYS_PACCT","CAP_SYS_PTRACE","CAP_SYS_RAWIO","CAP_SYS_RESOURCE","CAP_SYS_TIME","CAP_SYS_TTY_CONFIG","CAP_SYSLOG","CAP_WAKE_ALARM"];

let runtimeclusterpolicies = [];
let runtimecontainerpolicies = [];
let scanexecutionpolicies = [];
let scanresultpolicies = [];
let vulnerabilitymanagementpolicies = [];
let consolidatedpolicies = [];
let policytype="Runtime Container Policy";
let policytypestr="runtimecontainer";
if(groupLevel){
  policytype="Scan Execution Policy";
  policytypestr="scanexecution";
}
let editmode="rule";
let dastScanModalReferenceObject = null;
let dastSiteModalReferenceObject = null;

$(document).ready(function() {
  $("select").multiselect({
    buttonClass: "btn btn-link ml-3",
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true
  });

  switch (findGetParameter('type')) {
    case "runtimecontainer":
      if(!groupLevel) setPolicyType("Runtime Container Policy");
      break;
    case "runtimecluster":
      if(!groupLevel) setPolicyType("Runtime Cluster Policy");
      break;
    case "scanexecution":
      setPolicyType("Scan Execution Policy");
      break;
    case "scanresult":
      setPolicyType("Scan Result Policy");
      break;
    case "vulnerabilitymanagement":
      if(!groupLevel) setPolicyType("Vulnerability Management Policy");
      break;
  }
});

function getPolicyArr(){
  switch (policytypestr) {
    case "runtimecontainer":
      return runtimecontainerpolicies;
    case "runtimecluster":
      return runtimeclusterpolicies;
    case "scanexecution":
      return scanexecutionpolicies;
    case "scanresult":
      return scanresultpolicies;
    case "vulnerabilitymanagement":
      return vulnerabilitymanagementpolicies;
  }  
}

function repaintPolicy(obj){
  if(policytypestr=="scanexecution") {
    repaintPolicies();
  } else if(obj){

    if($(obj).children(".ruletype").children("button").html() == null){
      actiontype = $(obj).children(".actiontype").children("button").html() || $(obj).parent().children(".actiontypehidden").children("button").html();
      if(actiontype=="Pod Privileges" || actiontype=="Pod Capabilities" || actiontype=="System Configuration (sysctl)") {
        $(".ruletype").each(function(index,item){
          if($(item).children("button").html()=="A Pod attempts to start")
          obj=$(item).parent();
        });
      }
    }
    
    getPolicyArr()[$(obj).index()].dirty = true;
    repaintPolicies();
  }
}

function repaintPolicies(){
  let policyarr = getPolicyArr();

  let linkarr = [];

  for (let i=0;i<policyarr.length;i++){
    if(policyarr[i].policy instanceof NetworkPolicy) {
      linkarr.push({key: null, type: "NetworkPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
    } else if(policyarr[i].policy instanceof PSPPolicy) {
      linkarr.push({key: null, type: "PSPPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
    } else if (policyarr[i].policy instanceof AAPolicy){
      let found = false;
      let key = $($("#"+policytypestr+" .rulesection").children()[i]).children(".ruleprocess").children("button").html() == "any process" ? "/**" : $($("#"+policytypestr+" .rulesection").children()[i]).children(".rulefilterprocess").val();
      if(key=="") key="/**";
      for (j=0;!found && j<linkarr.length;j++){
        if(linkarr[j].key==key && linkarr[j].type=="AAPolicy"){
          linkarr[j].ruleId.push(i);
          linkarr[j].policy.push(policyarr[i].policy);
          linkarr[j].dirty = linkarr[j].dirty || policyarr[i].dirty;
          found = true;
        }
      }
      if(!found){
        linkarr.push({key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
      }
    } else if(policyarr[i].policy instanceof ScanExecutionPolicy) {
      linkarr.push({key: null, type: "ScanExecutionPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
    } else if(policyarr[i].policy instanceof ScanResultPolicy) {
      linkarr.push({key: null, type: "ScanResultPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
    } else if(policyarr[i].policy instanceof VulnerabilityManagementPolicy) {
      linkarr.push({key: null, type: "VulnerabilityManagementPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]});
    }
    policyarr[i].dirty = false;
  }

  switch (policytypestr) {
    case "runtimecontainer":
      runtimecontainerpolicies=policyarr;
      break;
    case "runtimecluster":
      runtimeclusterpolicies=policyarr;
      break;
    case "scanexecution":
      scanexecutionpolicies=policyarr;
      break;
    case "scanresult":
      scanresultpolicies=policyarr;
      break;
    case "vulnerabilitymanagement":
      vulnerabilitymanagementpolicies=policyarr;
      break;
  }

  let childcount = $("#preview"+policytypestr+"section").children().length;
  while(childcount>linkarr.length){
    $("#preview"+policytypestr+"section").children()[childcount-1].remove();
    childcount = $("#preview"+policytypestr+"section").children().length;
  }

  while(childcount<linkarr.length){
    previewblock = $('<div class="card previewsection"></div>').appendTo("#preview"+policytypestr+"section");
    $('<code class="previewcontent blue">' + colorize(defaultyaml)).appendTo(previewblock);
    childcount = $("#preview"+policytypestr+"section").children().length;
  }


  //Compare and repaint changed files
  for(let i=0;i<linkarr.length;i++){
    if(i>=consolidatedpolicies.length || linkarr[i].dirty || linkarr[i] != consolidatedpolicies[i]) {
      //repaint
      switch(linkarr[i].type){
        case "NetworkPolicy":
          generateNetworkPolicyPreview(i,linkarr[i]);
          generateRulePreview();
          break;
        case "PSPPolicy":
          generatePodPolicyPreview(i,linkarr[i]);
          generateRulePreview();
          break;
        case "AAPolicy":
          generateAppArmorPolicyPreview(i,linkarr[i]);
          generateRulePreview();
          break;
        case "ScanExecutionPolicy":
          generateScanExecutionPolicyPreview(i,linkarr[i]);
          generateScanExecutionRulePreview(i,linkarr[i]);
          break;
        case "ScanResultPolicy":
          generateScanResultPolicyPreview(i,linkarr[i]);
          generateRulePreview();
          break;
        case "VulnerabilityManagementPolicy":
          generateVulnerabilityManagementPolicyPreview(i,linkarr[i]);
          generateRulePreview();
          break;
      }
      linkarr[i].dirty = false;
    }
  }
  consolidatedpolicies = linkarr;
}

function setPolicyType(type){
  if(type!=policytype){
    if(type=="Vulnerability Management Policy"){
      $("#rulemodebutton").removeClass("editmodeleft");
      $("#yamlmodebutton").addClass("d-none");
    } else if (policytype=="Vulnerability Management Policy") {
      $("#rulemodebutton").addClass("editmodeleft");
      $("#yamlmodebutton").removeClass("d-none");
    }
    policytype=type;
    $("#"+policytypestr).addClass("d-none");
    policytypestr=policytype.toLowerCase().replace("policy","").replace(/ /g,"");
    $("#"+policytypestr).removeClass("d-none");
    setEditMode(editmode);
    $("#policytype").html(policytype);

    if($("#"+policytypestr+" .rulesection  .ruleblock:not(.newrule)").length==0){
      switch (type) {
        case "Runtime Container Policy":
          newRule('NetworkPolicy');
          break;
        case "Runtime Cluster Policy":
          newRule('PSPPolicy');
          break;
        case "Scan Execution Policy":
          newRule('ScanExecutionPolicy');
          break;
        case "Scan Result Policy":
          newRule('ScanResultPolicy');
          break;
        case "Vulnerability Management Policy":
          newRule('VulnerabilityManagementPolicy');
          break;
      }
    }
  }
}

function setEditMode(mode) {
  editmode=mode;
  if (mode == "rule") {
    $(".editmodeleft").addClass("pressed");
    $(".editmoderight").removeClass("pressed");
    $("#rulemode").css("display", "flex");
    $("#yamlmode").css("display", "none");
  } else if (mode == "yaml") {
    let yamltext = "";
    let editortext = "";
    let i=0;
    $("#preview"+policytypestr+"section").children().each(function(index,elem){
      yamltext += "<div class=\"card previewsection\">"+$(elem).html()+"</div>";
      text = $(elem).html().replaceAll(/<[^>]*>/g, "").replaceAll(" ","&nbsp;").replaceAll("\n","<br>");
      if(text.length>4 && text.substring(text.length-4,text.length)=="<br>") text=text.substring(0,text.length-4);
      editortext+= "<div class=\"card previewsection bg-transparent\"><code class=\"previewcontent bg-transparent\"><pre class=\"bg-transparent yamleditorcontent\" data-refid=\"yamleditor\" contenteditable>"+text+"</pre></code></div>";
      i++;
    });
    if(i==0){
      yamltext += "<div class=\"card previewsection\"><code class=\"previewcontent\"><pre class=\"blue\" style=\"color:gray\">Security policy yaml</pre></code></div>";
      editortext+= "<div class=\"card previewsection bg-transparent\"><code class=\"previewcontent bg-transparent\"><pre class=\"bg-transparent yamleditorcontent\" data-refid=\"yamleditor\" contenteditable></pre></code></div>";
    }
    $("#yamltext").html(yamltext);
    $("#yamleditor").html(editortext);
    $("#yamleditor").children().each(function(index, item){
      $(item).on("scroll", function(data){
        let textObj = $(data.target).parent().parent().parent().prev().children()[$(data.target).parent().parent().index()];
        $(textObj).children("code").children("pre").scrollLeft(data.target.scrollLeft);
      });
      observer.observe($(item).children("code").children("pre")[0],{childList: true, subtree: true, characterDataOldValue: true, characterData: true});
    });
    $("#yamlmode").children(".card")
      .each(function() {
        $(this).addClass("w-100");
      });
    $(".editmodeleft").removeClass("pressed");
    $(".editmoderight").addClass("pressed");
    $("#rulemode").css("display", "none");
    $("#yamlmode").css("display", "block");
  }
}

var observer = new MutationObserver(function( mutations ) {
  let targetObj = null;
  let repaint=false;
  mutations.every(function( mutation ) {
    if(mutation.target && mutation.target.dataset && mutation.target.dataset.refid){
      targetObj = mutation.target;
      return false;
    } else if(mutation.target && mutation.target.parentElement && mutation.target.parentElement.dataset && mutation.target.parentElement.dataset.refid){
      targetObj = mutation.target.parentElement;
      return false;
    } else if(mutation.target && mutation.target.parentElement && mutation.target.parentElement.parentElement && mutation.target.parentElement.parentElement.dataset && mutation.target.parentElement.parentElement.dataset.refid){
      targetObj = mutation.target.parentElement.parentElement;
      return false;
    } else {
      return true;
    }
  });
  if(targetObj){
    if(targetObj.dataset.refid=="repaint") repaint=true;
    let textObj = $(targetObj).parent().parent().parent().prev().children()[$(targetObj).parent().parent().index()];
    text = $(targetObj).html();
    if(text.indexOf("<pre")>-1){
      text=text.replaceAll(/<pre[^>]*>/g, "<br>").replaceAll(/<br><\/pre[^>]*>/g, "<br>").replaceAll(/<\/pre[^>]*><br>/g, "<br>").replaceAll(/<\/pre[^>]*>/g, "<br>");
      $(targetObj).html(text);
    }
    beginning="";
    ending="";
    if(text.length>=4 && text.substring(0,4)=="<br>") {
      beginning="&nbsp;";
    }
    if(text.length>=15 && text.substring(text.length-15,text.length)=="<div><br></div>") {
      ending="&nbsp;";
    }
    text=text.replaceAll("<div><br></div>","<br>").replaceAll("<br>","\n").replaceAll("<div>","\n").replaceAll("&nbsp;"," ").replaceAll(/<[^>]*>/g, "");
    if(text=="" || text=="\n"){
      text = "<pre class=\"blue\" style=\"color:gray;\">Security policy yaml</pre>";
    } else {
      text = "<pre class=\"blue\">" + beginning + colorize(text) + ending + "</pre>";
    }
    $(textObj).children("code").html(text);
    if(repaint) repaintPolicies();
  }
});

function newRule(ruletype) {
  switch(ruletype){
    case 'NetworkPolicy':
      defaultyaml="apiVersion: cilium.io/v2<br />kind: CiliumNetworkPolicy<br />metadata:<br />&nbsp;&nbsp;name: <br />&nbsp;&nbsp;description:";
      generatefunction=newNetworkPolicyRule;
      runtimecontainerpolicies.push({dirty: true, policy: new NetworkPolicy()});
      break;
    case 'PSPPolicy':
      defaultyaml="Unknown";
      generatefunction=newPSPPolicyRule;
      runtimeclusterpolicies.push({dirty: true, policy: new PSPPolicy()});
      break;
    case 'ScanExecutionPolicy':
      defaultyaml="Unknown";
      generatefunction=newScanExecutionPolicyRule;
      if(scanexecutionpolicies.length==0) scanexecutionpolicies.push({dirty: true, policy: new ScanExecutionPolicy()});
      break;
    case 'ScanResultPolicy':
      defaultyaml="Unknown";
      generatefunction=newScanResultPolicyRule;
      scanresultpolicies.push({dirty: true, policy: new ScanResultPolicy()});
      break;
    case 'VulnerabilityManagementPolicy':
      defaultyaml="Unknown";
      generatefunction=newVulnerabilityManagementPolicyRule;
      vulnerabilitymanagementpolicies.push({dirty: true, policy: new VulnerabilityManagementPolicy()});
      break;
  }

  ruleblock = $('<div class="ruleblock"></div>').insertBefore("#"+policytypestr+" .newrule");
  ruleblock = generatefunction(ruleblock);

  $('<div class="clear"></div>').appendTo(ruleblock);
  $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(ruleblock);
  counter++;
  updateActions();
  repaintPolicies();
}

function newAction() {
  return newAction2("alert");
}

function newAction2(type){
  let scanners = ["SAST","Secret Detection","DAST","Dependency Scanning","Container Scanning","License Compliance","API Fuzzing","Coverage Fuzzing"];
  actionblock = $('<div class="actionblock '+type+'action"></div>').insertBefore("#"+policytypestr+" .newaction");
  if ($("#"+policytypestr+" .actionsection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
  } else {
    $('<span class="rulejoiner ruleelement">AND&nbsp;</span>').appendTo(actionblock);
  }
  switch(type) {
    case "alert":
      actionblock = dropdownSelector(actionblock,"actiontypehidden",["alert"],null,false,APPENDTO);
      actionblock = staticText(actionblock, "send an Alert to GitLab");
      break;
    case "requirescan":
      actionblock = dropdownSelector(actionblock,"actiontypehidden",["requirescan"],null,false,APPENDTO);
      actionblock = staticText(actionblock, "Require a");

      //reduce down scanner list
      /*$("#"+policytypestr+" .actionsection .requirescanaction .actionscanner button").each(function (){
        const index = scanners.indexOf($(this).html());
        if(index>-1){
          scanners.splice(index,1);
        }
      });*/
      actionblock = dropdownSelector(actionblock,"actionscanner",scanners,"updateScanParameters(this,false);",false,APPENDTO);
      actionblock = staticText(actionblock, "scan to run");
      //scanners.splice(0,1);
      updateScanParameters($("#"+policytypestr+" .actionsection .requirescanaction .actionscanner .dropdown-menu").last().children().first(),true);
      limitScannerOptions();
      break;
    case "executeci":
      actionblock = dropdownSelector(actionblock,"actiontypehidden",["executeci"],null,false,APPENDTO);
      actionblock = staticText(actionblock, "Require a CI");
      actionblock = dropdownSelector(actionblock,"cireftype",["linked file","yaml block"],"updateCIRefType(this);",false,APPENDTO);
      actionblock = textboxInput(actionblock, "ciyamlfile", "project/ci-file.yaml", APPENDTO);
      actionblock = staticText(actionblock, "to run");
      break;
    case "requireapproval":
      actionblock = dropdownSelector(actionblock,"actiontypehidden",["requireapproval"],null,false,APPENDTO);
      let approvalgroups = ["Group A","Group B","Group C"];
      actionblock = staticText(actionblock, "Require approval from");
      actionblock = textboxInput(actionblock, "actionapprovalcount", "", APPENDTO);
      actionblock = staticText(actionblock, "or more individuals in the");
      actionblock = dropdownSelector(actionblock,"actionapprovalgroup",approvalgroups,null,false,APPENDTO);
      actionblock = staticText(actionblock, "approval group.");
      if(policytypestr!="vulnerabilitymanagement") actionblock = staticText(actionblock, "(Note: this only applies if a pipeline triggered the scan) ");
      break;
    case "slack":
      actionblock = dropdownSelector(actionblock,"actiontypehidden",["slack"],null,false,APPENDTO);
      actionblock = staticText(actionblock, "Send a Slack notification");
      actionblock = textboxInput(actionblock, "slackmessage", "message text", APPENDTO);
      actionblock = staticText(actionblock, "to the");
      actionblock = textboxInput(actionblock, "slackchannel", "#channel", APPENDTO);
      actionblock = staticText(actionblock, "channel");
      break;
    case "blockaction":
      actionblock = staticText(actionblock, "Block the action");
      break;
  }
  $('<div class="clear"></div>').appendTo(actionblock);
  $('<i class="fas fa-trash deleteicon" onClick="deleteAction(this);"></i>').appendTo(actionblock);

  if($("#"+policytypestr+" .newaction button").length>0 && (type!="requirescan" || scanners.length==0)){
      $("#"+policytypestr+" .newaction ."+type+"option").addClass("d-none");
  }
  
  if($("#"+policytypestr+" .newaction button").length>0){
    if($("#"+policytypestr+" .newaction .dropdown-menu a:not(.d-none)").length>0){
      $("#"+policytypestr+" .newaction").removeClass("d-none");
    } else {
      $("#"+policytypestr+" .newaction").addClass("d-none");
    }

  } else {
    $("#"+policytypestr+" .newaction").addClass("d-none");
  }
  repaintPolicies();
}

function limitScannerOptions(){
  let scanners = ["SAST","Secret Detection","Secret Detection","DAST","Dependency Scanning","Container Scanning","License Compliance","API Fuzzing","Coverage Fuzzing"];
  return scanners;
  //reduce down scanner list
  $("#"+policytypestr+" .actionsection .requirescanaction .actionscanner button").each(function (){
    const index = scanners.indexOf($(this).html());
    if(index>-1){
      scanners.splice(index,1);
    }
  });

  let allscanners = ["SAST","Secret Detection","DAST","Dependency Scanning","Container Scanning","License Compliance","API Fuzzing","Coverage Fuzzing"];
  $("#"+policytypestr+" .actionsection .requirescanaction .actionscanner").each(function (){
    text = $(this).children("button").html();
    $(this).children(".dropdown-menu").empty();
    allscanners.forEach((item) => {
      if(item==text || scanners.indexOf(item)>-1){
        $('<a class="dropdown-item" onclick="updateScanParameters(this,false);">'+item+'</a>').appendTo($(this).children(".dropdown-menu"));
      }
    });
  });

  return scanners;

}

function deleteAction(element) {
  const index = $(element).parent(".actionblock").index();
  type="";
  classes = $(element).parent().attr('class').split(/\s+/);
  classes.forEach(function(item){
    if(item!="actionblock" && item.indexOf("action")>-1 && item.lastIndexOf("action")==item.length-6){
      type = item.substring(0,item.length-6);
    }
  });
  if(type!=""){
    $("#"+policytypestr+" .newaction ."+type+"option").removeClass("d-none");
  }
  $(element).parent().remove();
  scanners=[];
  if(policytypestr=="scanexecution"){
    scanners = limitScannerOptions();
  }
  if($("#"+policytypestr+" .newaction button").length>0 && (type=="requirescan" || scanners.length>0)){
      $("#"+policytypestr+" .newaction ."+type+"option").removeClass("d-none");
  }
  
  if($("#"+policytypestr+" .newaction button").length>0){
    if($("#"+policytypestr+" .newaction .dropdown-menu a:not(.d-none)").length>0){
      $("#"+policytypestr+" .newaction").removeClass("d-none");
    } else {
      $("#"+policytypestr+" .newaction").addClass("d-none");
    }

  } else if($(element).parent().hasClass("alertaction")){
    $("#"+policytypestr+" .newaction").removeClass("d-none");
  }

  $("#"+policytypestr+" .actionsection").children()
    .each(function(index) {
      if (index == 0) {
        $(this).children(".rulejoiner").html("THEN&nbsp;");
      } else {
        $(this).children(".rulejoiner").html("AND&nbsp;");
      }
    });
  repaintPolicies();
}

var newNetworkPolicyRule = function newNetworkPolicyRule(ruleblock) {
  //IF Network Traffic is inbound to any pod in any namespace in the cluster and is inbound to a pod with the label X
  if ($("#"+policytypestr+" .rulesection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">IF&nbsp;</span>').appendTo(ruleblock);
  } else {
    $('<span class="rulejoiner ruleelement">OR&nbsp;</span>').appendTo(ruleblock);
  }
  ruleblock = dropdownSelector(ruleblock,"ruletype",["Network Traffic", "Files"],"updateContainerRuntimeRuleType(this);",false,APPENDTO);
  ruleblock = staticText(ruleblock, "is");
  ruleblock = dropdownSelector(ruleblock,"ruledirection",["inbound to", "outbound from"],"updateDirection(this);",false,APPENDTO);
  ruleblock = dropdownSelector(ruleblock,"rulepods",["any pod", "pods with label"],"updatePods(this);",false,APPENDTO);
  //ruleblock=staticText(ruleblock,"in");
  //ruleblock=dropdownSelector(ruleblock,"rulenamespaces",["any namespace","specific namespaces"],"updateNamespaces(this);",false,APPENDTO);
  ruleblock = staticText(ruleblock, "and is");
  $('<span class="ruledirectionphrase ruleelement">inbound from</span>').appendTo(ruleblock);
  ruleblock = dropdownSelector(ruleblock,"rulefilter",["anywhere", "an IP/subnet", "an entity", "a pod with the label"],"updateFilter(this);",false,APPENDTO);
  ruleblock = staticText(ruleblock, "on");
  ruleblock = dropdownSelector(ruleblock,"portfilter",["any port", "ports/protocols"],"updatePorts(this);",false,APPENDTO);
  return ruleblock;
}

var newPSPPolicyRule = function newPSPPolicyRule(ruleblock) {
  //IF Network Traffic is inbound to any pod in any namespace in the cluster and is inbound to a pod with the label X
  if ($("#"+policytypestr+" .rulesection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">IF&nbsp;</span>').appendTo(ruleblock);
  } else {
    $('<span class="rulejoiner ruleelement">OR&nbsp;</span>').appendTo(ruleblock);
  }
  ruleblock = dropdownSelector(ruleblock,"ruletype",["A Pod attempts to start"],null,false,APPENDTO);
  return ruleblock;
}

var newScanExecutionPolicyRule = function newScanExecutionPolicyRule(ruleblock) {
  //IF Network Traffic is inbound to any pod in any namespace in the cluster and is inbound to a pod with the label X
  if ($("#"+policytypestr+" .rulesection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">IF&nbsp;</span>').appendTo(ruleblock);
  } else {
    $('<span class="rulejoiner ruleelement">OR&nbsp;</span>').appendTo(ruleblock);
  }
  ruleblock = dropdownSelector(ruleblock,"ruletype",["A pipeline is run","Schedule"],"updateScanExecutionRuleType(this);",false,APPENDTO);
  ruleblock = staticText(ruleblock, "for the");
  ruleblock = multiselectSelector(ruleblock,"rulefilterprojects",["main","test","staging","big-edit"],APPENDTO);
  ruleblock = staticText(ruleblock, "branch(es)");
  return ruleblock;
}

var newScanResultPolicyRule = function newScanPolicyRule(ruleblock) {
  //IF Network Traffic is inbound to any pod in any namespace in the cluster and is inbound to a pod with the label X
  if ($("#"+policytypestr+" .rulesection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">IF&nbsp;</span>').appendTo(ruleblock);
  } else {
    $('<span class="rulejoiner ruleelement">OR&nbsp;</span>').appendTo(ruleblock);
  }
  ruleblock = dropdownSelector(ruleblock,"ruletype",["the security scanner(s)","the license scanner","a status check"],"updateScanResultRuleType(this);",false,APPENDTO);
  ruleblock = dropdownSelector(ruleblock,"rulescanners",["Any","SAST","Secret Detection","DAST","Dependency Scanning","Container Scanning","API Fuzzing","Coverage Fuzzing"],null,false,APPENDTO);
  ruleblock = staticText(ruleblock, "scan finds more than");
  ruleblock = textboxInput(ruleblock, "rulecount", "", APPENDTO);
  ruleblock = dropdownSelector(ruleblock,"ruleseverity",["Critical","High","Medium","Low","Unknown"],null,false,APPENDTO);
  ruleblock = dropdownSelector(ruleblock,"ruledetection",["Newly detected","Previously detected","Confirmed","Dismissed","Resolved"],null,false,APPENDTO);
  ruleblock = staticText(ruleblock, "vulnerabilities in an");
  ruleblock = staticText(ruleblock, "open merge");
  ruleblock = staticText(ruleblock, "request targeting");
  $('<input type="hidden" value="">').appendTo(ruleblock);
  ruleblock = multiselectSelector3(ruleblock,"rulebranches",["main","test","staging","big-edit"],[0,1,1,1],updateScanResultBranch,APPENDTO);
  return ruleblock;
}

var newVulnerabilityManagementPolicyRule = function newVulnerabilityManagementPolicyRule(ruleblock) {
  //IF Network Traffic is inbound to any pod in any namespace in the cluster and is inbound to a pod with the label X
  if ($("#"+policytypestr+" .rulesection").children().length == 3) {
    $('<span class="rulejoiner ruleelement">IF&nbsp;</span>').appendTo(ruleblock);
  } else {
    $('<span class="rulejoiner ruleelement">OR&nbsp;</span>').appendTo(ruleblock);
  }
  ruleblock = staticText(ruleblock, "A vulnerability");
  ruleblock = dropdownSelector(ruleblock,"ruletype",["is dismissed","changes severity"],null,false,APPENDTO);
  return ruleblock;
}

function dropdownSelector(ruleblock, label, options, clickaction, disabled, action) {
  if (action === APPENDTO) {
    selector = $('<div class="dropdown b-dropdown gl-dropdown mb-0 btn-group ' + label + ' ruleelement"></div>').appendTo(ruleblock);
  } else if (action == INSERTBEFORE) {
    selector = $('<div class="dropdown b-dropdown gl-dropdown mb-0 btn-group ' + label + ' ruleelement"></div>').insertBefore(ruleblock);
  } else if (action == INSERTAFTER) {
    selector = $('<div class="dropdown b-dropdown gl-dropdown mb-0 btn-group ' + label + ' ruleelement"></div>').insertAfter(ruleblock);
    ruleblock = selector;
  }
  $('<button class="btn dropdown-toggle  justify-content-between text-truncate" ID="' + label + counter + '"' + (disabled ? " disabled" : "") + ' type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (options.length > 0 ? options[0] : "") + "</button>").appendTo(selector);
  menu = $('<div class="dropdown-menu" aria-labelledby="' + label + counter + '"></div>').appendTo(selector);
  for (i = 0; i < options.length; i++) {
    $('<a class="dropdown-item" onClick="' + (clickaction ? clickaction : "updateText(this, false);") + '">' + options[i] + "</a>").appendTo(menu);
  }
  return ruleblock;
}


function multiselectSelector(ruleblock, label, options, action) {
  return multiselectSelector2(ruleblock, label, options, options, action);
}

function multiselectSelector2(ruleblock, label, options, values, action) {
  return multiselectSelector3(ruleblock, label, options, options, null, action);
}

function multiselectSelector3(ruleblock, label, options, values, change, action) {
  if (action === APPENDTO) {
    selector = $('<span class="' + label + ' ruleelement"></span>').appendTo(ruleblock);
  } else if (action == INSERTBEFORE) {
    selector = $('<span class="' + label + ' ruleelement"></span>').insertBefore(ruleblock);
  } else if (action == INSERTAFTER) {
    selector = $('<span class="' + label + ' ruleelement"></span>').insertAfter(ruleblock);
    ruleblock=selector;
  }
  menu = $('<select multiple="multiple"></select>').appendTo(selector);
  for (i = 0; i < options.length; i++) {
    $('<option value="' + values[i] + '">' + options[i] + "</option>").appendTo(menu);
  }
  $(menu).multiselect({
    onChange: function(option, checked, select) {
      if(change) change($(option).parent());
      repaintPolicy($(option).parent().parent().parent().parent());
    },
    onSelectAll: function() {
      //alert($(obj).prop("tagName"));
      if(change) change(this.$select[0]);
      getPolicyArr().forEach(function(item){ item.dirty = true; });
      repaintPolicies();
    },
    onDeselectAll: function() {
      //alert($(obj).prop("tagName"));
      if(change) change(this.$select[0]);
      getPolicyArr().forEach(function(item){ item.dirty = true; });
      repaintPolicies();
    },
    buttonClass: "btn btn-link",
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true
  });
  return ruleblock;
}

function staticText(ruleblock, txt) {
  $('<span class="ruleelement">' + txt + "&nbsp;</span>").appendTo(ruleblock);
  return ruleblock;
}

function textboxInput(ruleblock, label, placeholder, action) {
  if (action === APPENDTO) {
    $('<input type="text" class="form-control ' + label + ' ruleelement textboxelement" placeholder="' + placeholder + '" onKeyUp="repaintPolicy($(this).parent());">').appendTo(ruleblock);
  } else if (action == INSERTBEFORE) {
    $('<input type="text" class="form-control ' + label + ' ruleelement textboxelement" placeholder="' + placeholder + '" onKeyUp="repaintPolicy($(this).parent());">').insertBefore(ruleblock);
  } else if (action == INSERTAFTER) {
    $('<input type="text" class="form-control ' + label + ' ruleelement textboxelement" placeholder="' + placeholder + '" onKeyUp="repaintPolicy($(this).parent());">').insertAfter(ruleblock);
  }
  return ruleblock;
}

function updateText(element,repaintAll) {
  var text = $(element).html();
  $(element).parent().prev().html(text);
  if(repaintAll) repaintPolicies();
  else repaintPolicy($(element).parent().parent().parent());
}

function closeScanModal(){
  $(dastScanModalReferenceObject).val($('#scanmodalcontentbox').val());
  repaintPolicies();
}

function closeSiteModal(){
  $(dastSiteModalReferenceObject).val($('#sitemodalcontentbox').val());
  repaintPolicies();
}

function updateCIRefType(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (text == "linked file" && !$(nextdropdown).hasClass("ciyamlfile")) {
    $(nextdropdown).next().remove();
    actionblock = textboxInput(dropdown, "ciyamlfile", "project/ci-file.yaml", INSERTAFTER);
  }
  if (text == "yaml block" && !$(nextdropdown).hasClass("ciyamlblock")) {
    $(nextdropdown).remove();
    nextdropdown = $(dropdown).next();
    let yamlmode = $('<div class="yamlmode ruleelement w-100 mb-3 pb-0" style="background-color:white;line-height:1.5;"></div>').insertAfter(nextdropdown);
    let yamltext = $('<div class="yamltext"></div>').appendTo(yamlmode);
    let yamleditor = $('<div class="yamleditor w-100"></div>').appendTo(yamlmode);
    $(yamltext).html("<div class=\"card previewsection\"><code class=\"previewcontent\"><pre class=\"blue\" style=\"color:gray\">Security policy yaml</pre></code></div>");
    $(yamleditor).html("<div class=\"card previewsection bg-transparent\"><code class=\"previewcontent bg-transparent\"><pre class=\"bg-transparent yamleditorcontent\" data-refid=\"repaint\" contenteditable></pre></code></div>");
    $(yamleditor).on("scroll", function(data){
      let textObj = $(data.target).parent().parent().parent().prev().children()[$(data.target).parent().parent().index()];
      $(textObj).children("code").children("pre").scrollLeft(data.target.scrollLeft);
    });
    observer.observe($(yamleditor).children("div").children("code").children("pre")[0],{childList: true, subtree: true, characterDataOldValue: true, characterData: true});
  }
  updateText(element, true);
}

function updateDirection(element) {
  var text = $(element).html();
  if (text == "inbound to") {
    text = "inbound from";
  } else if (text == "outbound from") {
    text = "outbound to";
  }
  $(element).parent().parent().parent().children(".ruledirectionphrase").html(text);
  updateText(element, false);
}

function updatePods(element) {
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (
    text == "any pod" ||
    (text != "specific pods" && $(nextdropdown).hasClass("rulepod")) ||
    (text != "pods with label" && $(nextdropdown).hasClass("rulepodlabel"))
  ) {
    $(nextdropdown).remove();
  }
  if (text == "specific pods" && !$(nextdropdown).hasClass("rulepod")) {
    multiselectSelector(dropdown,"rulepod",["Pod A", "Pod B", "Pod C", "Pod D"],INSERTAFTER);
  } else if (text == "pods with label" && !$(nextdropdown).hasClass("rulepodlabel")) {
    textboxInput(dropdown, "rulepodlabel", "key: value", INSERTAFTER);
  }
  updateText(element, false);
}

function updateNamespaces(element) {
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (
    text == "any namespace" ||
    (text != "specific namespaces" && $(nextdropdown).hasClass("rulenamespace")) ||
    (text != "namespaces with label" && $(nextdropdown).hasClass("rulenamespacelabel"))
  ) {
    $(nextdropdown).remove();
  }
  if (text == "specific namespaces" && !$(nextdropdown).hasClass("rulenamespace")) {
    multiselectSelector(dropdown,"rulenamespace",["Namespace A", "Namespace B", "Namespace C", "Namespace D"],INSERTAFTER);
  } else if (text == "namespaces with label" && !$(nextdropdown).hasClass("rulenamespacelabel")) {
    textboxInput(dropdown, "rulenamespacelabel", "key: value", INSERTAFTER);
  }
  updateText(element, false);
}

function updateFilter(element) {
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (
    (text != "a pod with the label" && $(nextdropdown).hasClass("rulefilterpodlabel")) ||
    (text != "a service" && $(nextdropdown).hasClass("rulefilterservice")) ||
    (text != "an entity" && $(nextdropdown).hasClass("rulefilterentity")) ||
    (text != "an IP/subnet" && $(nextdropdown).hasClass("rulefilterip"))
  ) {
    $(nextdropdown).remove();
  }
  if (text == "a pod with the label" && !$(nextdropdown).hasClass("rulefilterpodlabel")) {
    textboxInput(dropdown, "rulefilterpodlabel", "key: value", INSERTAFTER);
  } else if (text == "a service" && !$(nextdropdown).hasClass("rulefilterservice")) {
    textboxInput(dropdown, "rulefilterservice", "service", INSERTAFTER);
  } else if (text == "an entity" && !$(nextdropdown).hasClass("rulefilterentity")) {
    multiselectSelector(dropdown,"rulefilterentity",["world", "init", "cluster", "remote-node", "host"],INSERTAFTER);
  } else if (text == "an IP/subnet" && !$(nextdropdown).hasClass("rulefilterip")) {
    textboxInput(dropdown, "rulefilterip", "0.0.0.0/24", INSERTAFTER);
  }
  updateText(element, false);
  //ruleblock=dropdownSelector(ruleblock,"rulepods",["any pod","pods with label"],"updatePods(this);",false,APPENDTO);
}

function updatePorts(element) {
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (text != "ports/protocols" && $(nextdropdown).hasClass("rulefilterport")) {
    $(nextdropdown).remove();
  }
  if (text == "ports/protocols" && !$(nextdropdown).hasClass("rulefilterport")) {
    textboxInput(dropdown, "rulefilterport", "80/tcp", INSERTAFTER);
  }
  updateText(element, false);
  //ruleblock=dropdownSelector(ruleblock,"rulepods",["any pod","pods with label"],"updatePods(this);",false,APPENDTO);
}

function updateProcess(element) {
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown = $(dropdown).next();
  //Remove incorrect dropdowns
  if (text != "specific processes" && $(nextdropdown).hasClass("rulefilterprocess")) {
    $(nextdropdown).remove();
  }
  if (text == "specific processes" && !$(nextdropdown).hasClass("rulefilterprocess")) {
    textboxInput(dropdown, "rulefilterprocess", "", INSERTAFTER);
  }
  updateText(element, false);
  //ruleblock=dropdownSelector(ruleblock,"rulepods",["any pod","pods with label"],"updatePods(this);",false,APPENDTO);
}

function updateUserOrGroup(element,type){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  var nextdropdown1 = $(dropdown).next();
  var nextdropdown2 = $(dropdown).next().next();
  //Remove incorrect dropdowns
  if (text != "specific "+type+"s" && $(nextdropdown2).hasClass("action"+type+"max")) {
    $(nextdropdown2).remove();
  }
  if (text != "specific "+type+"s" && $(nextdropdown1).hasClass("action"+type+"min")) {
    $(nextdropdown1).remove();
  }
  if (text == "specific "+type+"s" && !$(nextdropdown1).hasClass("action"+type+"min")) {
    textboxInput(dropdown, "action"+type+"min", "min "+type+" ID", INSERTAFTER);
  }
  if (text == "specific "+type+"s" && !$(nextdropdown2).hasClass("action"+type+"max")) {
    textboxInput($(dropdown).next(), "action"+type+"max", "max "+type+" ID", INSERTAFTER);
  }
  updateText(element, false);
}

function updateActionTarget(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();

  if(text!=oldtext){
    $(dropdown).next().remove();
    switch(text){
      case "cluster":
        dropdown = dropdownSelector(dropdown,"rulecluster",["production","www-gitlab-com","version-gitlab-com","license-gitlab-com"],"updateText(this,false);",false,INSERTAFTER);
        dropdown = $('<span class="ruleelement">in namespaces&nbsp;</span>').insertAfter(dropdown);
        multiselectSelector(dropdown,"rulenamespace",["default","kube-node-lease","kube-public","kube-system"],INSERTAFTER);
        break;
      case "branch":
        $(dropdown).next().remove();
        $(dropdown).next().remove();
        dropdownSelector(dropdown,"rulebranch",["main","test","staging","big-edit"],"updateText(this,false);",false,INSERTAFTER);
        break;
    }
    updateText(element, false);
  }
}

function updateFrequency(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();

  if(text!=oldtext){
    $(dropdown).nextAll().remove();
    switch(text){
      case "hourly":
        container = staticText(container, "at");
        container = multiselectSelector(container,"rulehours",["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"],APPENDTO);
        break;
      case "daily":
        container = staticText(container, "at");
        container = dropdownSelector(container,"rulehours",["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"],null,false,APPENDTO);
        break;
      case "weekly":
        container = staticText(container, "on");
        container = dropdownSelector(container,"ruledays",["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],null,false,APPENDTO);
        container = staticText(container, "at");
        container = dropdownSelector(container,"rulehours",["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"],null,false,APPENDTO);
        break;
      case "monthly":
        break;
    }
    $('<div class="clear"></div>').appendTo(container);
    $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    updateText(element, false);
  }
}

function updateScanParameters(element, force){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();
  var hastrash = $(container).children().last().hasClass("fa-trash");

  if(text!=oldtext || force){
    $(dropdown).nextAll().remove();
    container = staticText(container, "scan to run");
    switch(text){
      case "SAST":
        break;
      case "DAST":
        container = staticText(container, "with scan profile");
        container = dropdownSelector(container,"actiondastscanprofile",["--Select Scan Profile--","Create New Scan Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>","Scan Profile A","Scan Profile B"],"updateDASTScanProfile(this);",false,APPENDTO);
        container = textboxInput(container, "scanprofiledatablock", "", APPENDTO);
        container = staticText(container, "and site profile");
        container = dropdownSelector(container,"actiondastsiteprofile",["--Select Site Profile--","Create New Site Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>","Site Profile A","Site Profile B"],"updateDASTSiteProfile(this);",false,APPENDTO);
        container = textboxInput(container, "siteprofiledatablock", "", APPENDTO);
        dastScanModalReferenceObject = $(container).children(".scanprofiledatablock");
        dastSiteModalReferenceObject = $(container).children(".siteprofiledatablock");
//        container = staticText(container, "and scanner profile");
//        container = dropdownSelector(container,"actiondastprofile",["--Select Profile--","Create New Profile","Profile A","Profile B"],"updateDASTProfile(this);",false,APPENDTO);
        break;
      default:
        break;
    }
    $('<div class="clear"></div>').appendTo(container);
    if(hastrash) $('<i class="fas fa-trash deleteicon" onClick="deleteAction(this);"></i>').appendTo(container);
    updateText(element, true);
    limitScannerOptions();
  }
}

function updateDASTScanProfile(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();
  switch(text){
    case "Create New Scan Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>":
      $('#scanmodalcontentbox').val($(dastScanModalReferenceObject).val());
      $('#dastscanprofilemodal').modal('show');
      break;
  }
  if(text.substring(0,15)=="Create New Scan Profile") {
    $(dropdown).children("button").html("--Select Scan Profile--");
  } else {
    updateText(element, true);
  }
}

function updateDASTSiteProfile(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();
  switch(text){
    case "Create New Site Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>":
      $('#sitemodalcontentbox').val($(dastSiteModalReferenceObject).val());
      $('#dastsiteprofilemodal').modal('show');
      break;
  }
  if(text.substring(0,15)=="Create New Site Profile") {
    $(dropdown).children("button").html("--Select Site Profile--");
  } else {
    updateText(element, true);
  }
}

function updateContainerRuntimeRuleType(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();

  if(text!=oldtext){
    $(dropdown).nextAll().remove();
    switch(text) {
      case "Network Traffic":
        container = staticText(container, "is");
        container = dropdownSelector(container,"ruledirection",["inbound to", "outbound from"],"updateDirection(this);",false,APPENDTO);
        container = dropdownSelector(container,"rulepods",["any pod", "pods with label"],"updatePods(this);",false,APPENDTO);
        container = staticText(container, "and is");
        $('<span class="ruledirectionphrase ruleelement">inbound from</span>').appendTo(container);
        container = dropdownSelector(container,"rulefilter",["anywhere", "an IP/subnet", "an entity", "a pod with the label"],"updateFilter(this);",false,APPENDTO);
        container = staticText(container, "on");
        container = dropdownSelector(container,"portfilter",["any port", "ports/protocols"],"updatePorts(this);",false,APPENDTO);

        defaultyaml="apiVersion: cilium.io/v2<br />kind: CiliumNetworkPolicy<br />metadata:<br />&nbsp;&nbsp;name: <br />&nbsp;&nbsp;description:";
        runtimecontainerpolicies[$(container).index()] = {dirty: true, policy: new NetworkPolicy()};
        break;
      case "Files":
        container = staticText(container, "are ");
        container = multiselectSelector2(container,"rulepermissions",["read","written","executed"],["r","w","x"],APPENDTO);
        container = staticText(container, "by ");
        container = dropdownSelector(container,"ruleprocess",["any process", "specific processes"],"updateProcess(this);",false,APPENDTO);
        container = staticText(container, "where the file path and name match ");
        container = textboxInput(container, "rulefile", "", APPENDTO);

        defaultyaml="profile {<br />}";
        runtimecontainerpolicies[$(container).index()] = {dirty: true, policy: new AAPolicy()};
        break;
    }
    $('<div class="clear"></div>').appendTo(container);
    $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    updateText(element, false);
    updateActions();
  }
}

function updateScanExecutionRuleType(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();

  if(text!=oldtext){
    $(dropdown).nextAll().remove();
    switch(text) {
      case "A pipeline is run":
      case "A commit is merged":
        container = staticText(container, "for the");
        container = multiselectSelector(container,"rulefilterprojects",["main","test","staging","big-edit"],APPENDTO);
        container = staticText(container, "branch(es)");
        break;
      case "Schedule":
        container = staticText(container, "actions for the");
        container = dropdownSelector(container,"ruletarget",["branch","cluster"],"updateActionTarget(this);",false,APPENDTO);
        container = dropdownSelector(container,"rulebranch",["main","test","staging","big-edit"],null,false,APPENDTO);
        container = dropdownSelector(container,"rulefrequency",["daily","weekly"],"updateFrequency(this);",false,APPENDTO);
        container = staticText(container, "at");
        container = dropdownSelector(container,"rulehours",["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"],null,false,APPENDTO);
        break;
    }
    $('<div class="clear"></div>').appendTo(container);
    $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    updateText(element, false);
  }
}

function updateScanResultRuleType(element){
  var text = $(element).html();
  dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  var oldtext = $(dropdown).children("button").html();

  if(text!=oldtext){
    if (text == "the security scanner(s)" && oldtext != "the security scanner(s)") {
      $(dropdown).nextAll().remove();
      ruleblock = dropdownSelector(ruleblock,"rulescanners",["Any","SAST","Secret Detection","DAST","Dependency Scanning","Container Scanning","API Fuzzing","Coverage Fuzzing"],null,false,APPENDTO);
      ruleblock = staticText(ruleblock, "scan finds more than");
      ruleblock = textboxInput(ruleblock, "rulecount", "", APPENDTO);
      ruleblock = dropdownSelector(ruleblock,"ruleseverity",["Critical","High","Medium","Low","Unknown"],null,false,APPENDTO);
      ruleblock = dropdownSelector(ruleblock,"ruledetection",["Newly detected","Previously detected","Confirmed","Dismissed","Resolved"],null,false,APPENDTO);
      ruleblock = staticText(ruleblock, "vulnerabilities in an");
      ruleblock = staticText(ruleblock, "open merge");
      ruleblock = staticText(ruleblock, "request targeting");
      $('<input type="hidden" value="">').appendTo(ruleblock);
      ruleblock = multiselectSelector3(ruleblock,"rulebranches",["main","test","staging","big-edit"],[0,1,1,1],updateScanResultBranch,APPENDTO);
      $('<div class="clear"></div>').appendTo(container);
      $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    } else if (text == "the license scanner" && oldtext != "the license scanner") {
      $(dropdown).nextAll().remove();
      container = staticText(container, "finds more than");
      container = textboxInput(container, "rulecount", "", APPENDTO);
      container = multiselectSelector(container,"rulelicenses",["MIT","BSD-2","BSD-3","APL","GPL"],APPENDTO);
      container = staticText(container, "licenses that are");
      container = multiselectSelector(container,"ruledetection",["Newly detected","Pre-existing"],APPENDTO);
      $('<div class="clear"></div>').appendTo(container);
      $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    } else if (text == "a status check" && oldtext != "a status check") {
      $(dropdown).nextAll().remove();
      container = staticText(container, "with a name matching");
      container = textboxInput(container, "rulestatusname", "*", APPENDTO);
      container = staticText(container, "has a status of");
      container = dropdownSelector(container,"ruleseverity",["Failed","Pending","Passed"],null,false,APPENDTO);
      $('<div class="clear"></div>').appendTo(container);
      $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
    }

    updateText(element, false);
  }
}

let updateScanResultBranch = function updateScanResultBranch(element){
  const options = $(element).val();
  let dropdown = $(element).parent().parent();
  container = $(dropdown).parent();
  const oldOptions = $(dropdown).prev().val();
  if(options.includes("0") && oldOptions.indexOf("0")==-1) {
    $(dropdown).nextAll().remove();
    container = staticText(container, "that are");
    container = multiselectSelector(container,"ruledetection",["newly detected","re-introduced (pre-existing resolved)","pre-existing dismissed","pre-existing detected","pre-existing confirmed"],APPENDTO);
    $('<div class="clear"></div>').appendTo(container);
    $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
  } else if(!options.includes("0") && oldOptions.indexOf("0")>=0){
    $(dropdown).nextAll().remove();
    $('<div class="clear"></div>').appendTo(container);
    $('<i class="fas fa-trash deleteicon" onClick="deleteRule(this);"></i>').appendTo(container);
  }
  $(dropdown).prev().val(options);

  //ruleblock = staticText(ruleblock, "that are");
  //ruleblock = multiselectSelector(ruleblock,"ruledetection",["newly detected","re-introduced (pre-existing resolved)","pre-existing dismissed","pre-existing detected","pre-existing confirmed"],APPENDTO);

}

function deleteRule(element) {
  const index = $(element).parent(".ruleblock").index();
  switch(policytypestr){
    case 'runtimecontainer':
      runtimecontainerpolicies.splice(index, 1);
      break;
    case 'runtimecluster':
      runtimeclusterpolicies.splice(index, 1);
      break;
    case 'scanexecution':
      if(countRules()==1) scanexecutionpolicies.splice(index, 1);
      break;
  }
  $(element).parent().remove();
  $("#"+policytypestr+" .rulesection").children().each(function(index) {
      if (index == 0) {
        $(this).children(".rulejoiner").html("IF&nbsp;");
      } else {
        $(this).children(".rulejoiner").html("OR&nbsp;");
      }
    });
  updateActions();
  repaintPolicies();
}

function countRules(){
  return $("#"+policytypestr+" .rulesection").children().length-1;
}

function generateNetworkPolicyPreview(index, item) {
  policy = item.policy[0];
  ruleblock = $("#"+policytypestr+" .rulesection").children()[item.ruleId[0]];
  if (!policy) return;

  const filter3 = $(ruleblock).children(".rulefilter").children("button").html();
  const filter4 = $(ruleblock).children(".portfilter").children("button").html();

  policy.name = $("#"+policytypestr+"policyname").val()+"-"+index;
  policy.description = {"app.gitlab.com/policy-description": $("#"+policytypestr+"policydescription").val()};
  if($("#"+policytypestr+" .alertaction").length > 0){
    policy.description['app.gitlab.com/alert']=true;
  }
  if(!$("#"+policytypestr+"policyenabled").prop("checked")){
    policy.description['network-policy.gitlab.com/disabled_by']="gitlab";
  }

  const direction = $(ruleblock).children(".ruledirection").children("button").html() === "inbound to" ? PolicyDirectionInbound : PolicyDirectionOutbound;
  policy.direction = direction;

  let rule3 = null;
  let rule4 = null;
  switch (filter3) {
    case "a pod with the label":
      const labels = $(ruleblock).children(".rulefilterpodlabel").val().split(",")
        .reduce((acc, val) => {
          const components = val.split(":");
          if (components.length === 2)
            acc[components[0].trim()] = components[1].trim();
          return acc;
        }, {});
      rule3 = new RuleLabel(labels, direction);
      break;
    case "a service":
      const service = $(ruleblock).children(".rulefilterservice").val();
      rule3 = new RuleService(service, direction);
      break;
    case "an entity":
      const entities = $(ruleblock).children(".rulefilterentity").find("select").val();
      rule3 = new RuleEntity(entities.length === 5 ? "all" : entities,direction);
      break;
    case "an IP/subnet":
      const cidr = $(ruleblock).children(".rulefilterip").val();
      rule3 = new RuleCIDR(cidr, direction);
      break;
  }
  switch (filter4) {
    case "ports/protocols":
      ports = $(ruleblock).children(".rulefilterport").val().split(",");
      ports.forEach(function(item,index,array){
             const components = item.split("/");
             if (components.length >= 2 && components[1].trim().length>0)
               array[index]= {"port": components[0].trim(),"protocol": components[1].trim().toUpperCase()};
             else if (components.length >= 1)
               array[index]= {"port": components[0].trim(),"protocol": "ANY"};
      });
      rule4 = new RulePort(ports,direction);
      break;
  }
  policy.rule3 = rule3;
  policy.rule4 = rule4;

  const rulepods = $(ruleblock).children(".rulepods").children("button").html();
  if (rulepods == "pods with label") {
    const labels = $(ruleblock).children(".rulepodlabel").val().split(",")
      .reduce((acc, val) => {
        const components = val.split(":");
        if (components.length === 2)
          acc[components[0].trim()] = components[1].trim();
        return acc;
      }, {});
    policy.endpointSelectorLabels = labels;
  }

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toYaml())}</pre>`);
}

function generatePodPolicyPreview(index, item) {
  policy = item.policy[0];
  if (!policy) return;

  policy.name = $("#"+policytypestr+"policyname").val()+"-"+index;
  policy.description = {"app.gitlab.com/policy-description": $("#"+policytypestr+"policydescription").val()};
  if($("#"+policytypestr+" .alertaction").length > 0){
    policy.description['app.gitlab.com/alert']=true;
  }
  if(!$("#"+policytypestr+"policyenabled").prop("checked")){
    policy.description['cluster-policy.gitlab.com/disabled_by']="gitlab";
  }

  $(".actionsection").children(".mandatory").each(function(index,item){
    actiontype=$(item).children(".actiontype").children("button").html() || $(item).children(".actiontypehidden").children("button").html();
    switch(actiontype){
      case "Pod Privileges":
        actionuser=$(item).children(".actionuser").children("button").html();
        actiongroup=$(item).children(".actiongroup").children("button").html();
        userRule="";
        userMin=null;
        userMax=null;
        groupRule="";
        groupMin=null;
        groupMax=null;

        switch(actionuser){
          case "any user":
            userRule="RunAsAny";
            break;
          case "any user except root":
            userRule="MustRunAsNonRoot";
            policy.privileged = false;
            break;
          case "specific users":
            userRule="MustRunAs";
            userMin=parseInt($(item).children(".actionusermin").val());
            if(isNaN(userMin)) userMin=0;
            userMax=parseInt($(item).children(".actionusermax").val());
            if(isNaN(userMax)) userMax=65535;
            break;
        }

        switch(actiongroup){
          case "any group":
            groupRule="RunAsAny";
            break;
          case "specific groups":
            groupRule="MustRunAs";
            groupMin=parseInt($(item).children(".actiongroupmin").val());
            if(isNaN(groupMin)) groupMin=0;
            groupMax=parseInt($(item).children(".actiongroupmax").val());
            if(isNaN(groupMax)) groupMax=65535;
            break;
        }

        escalation=$(item).children(".actionprivescalation").children("button").html();
        if(escalation=="allow") {
          policy.privilegeEscalation = true;
        } else if(escalation=="block"){
          policy.privilegeEscalation = false;
        }

        policy.runAsUser=new RuleUserOrGroup(userRule,userMin,userMax);
        policy.runAsGroup=new RuleUserOrGroup(groupRule,groupMin,groupMax);
        break;
      case "Pod Capabilities":
        policy.allowedCapabilities = $(item).children(".actionallowedcapabilities").find("select").val().length==capabilities.length ? ["All"] : $(item).children(".actionallowedcapabilities").find("select").val();
        policy.requiredDropCapabilities = $(item).children(".actionrequireddropcapabilities").find("select").val().length==capabilities.length ? ["All"] : $(item).children(".actionrequireddropcapabilities").find("select").val();
        policy.defaultAddCapabilities = $(item).children(".actiondefaultaddcapabilities").find("select").val().length==capabilities.length ? ["All"] : $(item).children(".actiondefaultaddcapabilities").find("select").val();
        break;
      case "System Configuration (sysctl)":
        policy.forbiddenSysctls = $(item).children(".actionforbiddensysctls").val().split(",").map(str => str.trim());;
        policy.allowedUnsafeSysctls = $(item).children(".actionallowedunsafesysctls").val().split(",").map(str => str.trim());
        break;
    }
  });

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toYaml())}</pre>`);
}

function generateAppArmorPolicyPreview(index, item) {
  //{key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]}
  policy = item.policy[0];
  ruleblock = $("#"+policytypestr+" .rulesection").children()[item.ruleId[0]];
  if (!policy) return;

  policy.name = $("#"+policytypestr+"policyname").val()+"-"+index;
  policy.description = $("#"+policytypestr+"policydescription").val();
  policy.mode = $("#filesaction").find(".actiontype").val()=="allow"?"audit":"audit deny";
  policy.process = item.key;
  fileRules=[];
  for(let i=0;i<item.ruleId.length;i++){
    let permissions = $(ruleblock).children(".rulepermissions").find("select").val()+"";
    permission = permissions.replaceAll(",","");
    let file = $(ruleblock).children(".rulefile").val();
    fileRules.push(new FileRule(permissions,file));
  }
  policy.fileRules = fileRules;

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toText())}</pre>`);
}

function generateScanExecutionPolicyPreview(index, item) {
  //{key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]}
  policy = item.policy[0];
  ruleblock = $("#"+policytypestr+" .rulesection").children()[item.ruleId[0]];
  if (!policy) return;
  policy.name = $("#"+policytypestr+"policyname").val();
  if ($("#"+policytypestr+"policyname").val().length>0){
    policy.description = $("#"+policytypestr+"policydescription").val();
  }
  if(groupLevel){
    if($("#"+policytypestr+"policyprojectlabels").val().length==7) policy.projectlabels=["*"];
    else policy.projectlabels=$("#"+policytypestr+"policyprojectlabels").val();
  }
  policy.enabled=$("#"+policytypestr+"policyenabled").prop("checked");
  
  policy.rules=[];
  $("#"+policytypestr+" .rulesection").children(".ruleblock:not(.newrule)").each(function(index,elem){
    switch ($(elem).children(".ruletype").children("button").html()){
      case "A pipeline is run":
        branches = $(elem).children(".rulefilterprojects").find("select").val();
        policy.rules.push({type: "pipeline", branches: branches});
        break;
      case "A commit is merged":
        branches = $(elem).children(".rulefilterprojects").find("select").val();
        policy.rules.push({type: "commit", branches: branches});
        break;
      case "Schedule":
        cadence = "";
        frequency = $(elem).children(".rulefrequency").find("button").html();
        switch(frequency){
          case "daily":
            hours = $(elem).children(".rulehours").find("button").html();
            hours = parseInt(hours.substring(0,hours.indexOf(":")));
            cadence = "0 "+hours+" * * *";
            break;
          case "weekly":
            hours = $(elem).children(".rulehours").find("button").html();
            hours = parseInt(hours.substring(0,hours.indexOf(":")));
            weekday = $(elem).children(".ruledays").find("button").html();
            switch(weekday){
              case "Sunday":
                days=0;
                break;
              case "Monday":
                days=1;
                break;
              case "Tuesday":
                days=2;
                break;
              case "Wednesday":
                days=3;
                break;
              case "Thursday":
                days=4;
                break;
              case "Friday":
                days=5;
                break;
              case "Saturday":
                days=6;
                break;
            }
            cadence = "0 "+hours+" * * "+days;
            break;
        }
        target = $(elem).children(".ruletarget").find("button").html();
        switch(target){
          case "branch":
            branches = $(elem).children(".rulebranch").find("button").html();
            policy.rules.push({type: "schedule", branches: branches, cadence: cadence});
            break;
          case "cluster":
            cluster = $(elem).children(".rulecluster").find("button").html();
            clusters = {};
            if($(elem).children(".rulenamespace").find("select").children().length==$(elem).children(".rulenamespace").find("select").val().length) {
              clusters[cluster] = {namespaces: ["*"]};
            } else {
              clusters[cluster] = {namespaces: $(elem).children(".rulenamespace").find("select").val()};
            }
            policy.rules.push({type: "schedule", clusters: clusters, cadence: cadence});
            break;
        }
        break;
      default:
        return;
    }
  });

  policy.actions=[];
  $("#"+policytypestr+" .actionsection").children(".actionblock:not(.newaction)").each(function(index,elem){
    switch ($(elem).children(".actiontypehidden").children("button").html()){
      case "alert":
        policy.actions.push({alert: true});
        break;
      case "requirescan":
        scanner = $(elem).children(".actionscanner").find("button").html();
        switch(scanner){
          case "DAST":
            scanprofile = $(elem).children(".actiondastscanprofile").find("button").html();
            siteprofile = $(elem).children(".actiondastsiteprofile").find("button").html();
            if(scanprofile=="--Select Scan Profile--") scanprofile="";
            if(siteprofile=="--Select Site Profile--") siteprofile="";
            if(scanprofile=="Create New Scan Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>") scanprofile=String($(elem).children(".scanprofiledatablock").val());
            if(siteprofile=="Create New Site Profile&nbsp;&nbsp;<svg width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\"><g id=\"external_link\" class=\"icon_svg-stroke\" stroke=\"#666\" stroke-width=\"1.5\" fill=\"none\" fill-rule=\"evenodd\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><polyline points=\"17 13.5 17 19.5 5 19.5 5 7.5 11 7.5\"></polyline><path d=\"M14,4.5 L20,4.5 L20,10.5 M20,4.5 L11,13.5\"></path></g></svg>") siteprofile=$(elem).children(".siteprofiledatablock").val();
            policy.actions.push({scan: scanner, scanprofile: scanprofile, siteprofile: siteprofile});
            break;
          default:
            policy.actions.push({scan: scanner});
            break;
        }
        break;
      case "executeci":
        cireftype = $(elem).children(".cireftype").find("button").html();
        switch(cireftype){
          case "linked file":
            cifile = $(elem).children(".ciyamlfile").val();
            policy.actions.push({cifile: cifile});
            break;
          case "yaml block":
            ciblock = String($($(elem).find(".yamleditorcontent")[0]).html());
            policy.actions.push({ciblock: ciblock});
            break;
        }
        break;
      case "slack":
        message = $(elem).children(".slackmessage").val();
        channel = $(elem).children(".slackchannel").val();
        policy.actions.push({slack: {message: message, channel: channel}});
        break;
      default:
        return;
    }
  });

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toYaml())}</pre>`);
}

function generateScanResultPolicyPreview(index, item) {
  //{key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]}
  policy = item.policy[0];
  ruleblock = $("#"+policytypestr+" .rulesection").children()[item.ruleId[0]];
  if (!policy) return;

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toYaml())}</pre>`);
}

function generateVulnerabilityManagementPolicyPreview(index, item) {
  //{key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]}
  policy = item.policy[0];
  ruleblock = $("#"+policytypestr+" .rulesection").children()[item.ruleId[0]];
  if (!policy) return;

  $($("#preview"+policytypestr+"section").children()[index]).find(".previewcontent").html(`<pre class="blue">${colorize(policy.toYaml())}</pre>`);
}

function generateRulePreview() {
  text = "";
  start = "<span class=\"previewcontent\">";
  end = "</span>";
  
  networkblocks=[];
  podblocks=[];
  $("#"+policytypestr+" .rulesection").children(".ruleblock:not(.newrule)").each(function(index,item){
    if ($(item).children(".ruletype")) {
      ruletype=$(item).children(".ruletype").children("button").html();
      switch(ruletype) {
        case "Network Traffic":
          networkblocks.push(item);
          break;
        case "A Pod attempts to start":
          podblocks.push(item);
          break;
      }
    }
  });

  if(networkblocks.length>0) text += start + generateNetworkRulePreview(networkblocks) + end;
  if(podblocks.length>0) text += start + generatePodRulePreview(podblocks) + end;

  if (text == "") {
    text = start + "No rules defined yet." + end;
  }

  $("#preview"+policytypestr+"rule").find(".previewbox").html(text);
}

function generateNetworkRulePreview(ruleblocks) {
  text="";
  for (i=0;i<ruleblocks.length;i++){
    ruleblock=ruleblocks[i];
    if (i > 0) {
      text += "<br /><br /><b>AND</b><br /><br />";
    }
    text += "<b>Allow</b> all";
    direction = $(ruleblock).children(".ruledirection").children("button").html();
    text += direction == "inbound to" ? " <b>inbound</b> traffic to" : " <b>outbound</b> traffic from";
    rulepods = $(ruleblock).children(".rulepods").children("button").html();
    if (rulepods == "any pod") {
      text += " <b>all pods</b>";
    } else if (rulepods == "pods with label") {
      text += " pods <b>[" + $(ruleblock) .children(".rulepodlabel") .val() + "]</b>";
    }
    /*text+=" in"
          rulenamespaces = $(ruleblock).children(".rulenamespaces").children("button").html();
          if(rulenamespaces=="any namespace") {
              text+=" <b>all namespaces</b>";
          } else if (rulenamespaces=="specific namespaces"){
              namespaces=$(ruleblock).children(".rulenamespace").find("select").val();
              if(namespaces=="") namespaces=" <b>no namespaces</b>";
              text+=" namespaces <b>["+namespaces+"]</b>";
          }*/

    filter3 = $(ruleblock).children(".rulefilter").children("button").html();
    filter4 = $(ruleblock).children(".portfilter").children("button").html();
    if (filter3 == "anywhere") {
      text += " <b>anywhere</b>";
    } else if (filter3 == "a pod with the label") {
      text += direction == "inbound to" ? " from" : " to";
      if ($(ruleblock).children(".rulefilterpodlabel").val() == "") {
        text += " <b>no allowed pods</b>";
      } else {
        text += " pods <b>[" + $(ruleblock).children(".rulefilterpodlabel").val() + "]</b>";
      }
    } else if (filter3 == "a service") {
      text += direction == "inbound to" ? " from" : " to";
      if ($(ruleblock).children(".rulefilterservice").val() == "") {
        text += " <b>no allowed services</b>";
      } else {
        text += " the service <b>" + $(ruleblock).children(".rulefilterservice").val() + "</b>";
      }
    } else if (filter3 == "an entity") {
      text += direction == "inbound to" ? " from" : " to";
      entities = $(ruleblock).children(".rulefilterentity").find("select").val();
      if (entities.length == 0) {
        text += " <b>nowhere</b>";
      } else if (entities.length == 5) {
        text += " <b>anywhere</b>";
      } else {
        text += " <b>[";
        for (i = 0; i < entities.length; i++) {
          if (i > 0) text += ", ";
          if (entities[i] == "world") {
            text += "the world";
          } else if (entities[i] == "init") {
            text += "init";
          } else if (entities[i] == "cluster") {
            text += "inside the cluster";
          } else if (entities[i] == "remote-node") {
            text += "outside the cluster";
          } else if (entities[i] == "host") {
            text += "the host";
          }
        }
        text += "]</b>";
      }
    } else if (filter3 == "an IP/subnet") {
      text += direction == "inbound to" ? " from" : " to";
      if ($(ruleblock).children(".rulefilterip").val() == "") {
        text += " <b>no allowed IP addresses</b>";
      } else {
        text +=" <b>" + $(ruleblock).children(".rulefilterip").val() + "</b>";
      }
    }
    text+=" on";
    if (filter4 == "any port") {
      text+=" <b>any port</b>";
    } else if (filter4 == "ports/protocols") {
      if ($(ruleblock).children(".rulefilterport").val() == "") {
        text += " <b>no allowed ports</b>";
      } else {
        text += " <b>port" + ($(ruleblock).children(".rulefilterport").val().split(",").length>1?"s ":" ") + $(ruleblock).children(".rulefilterport").val() + "</b>";
      }
    }
  }
  return text;
}

function generatePodRulePreview(ruleblock){
  return "This code is not yet implemented";
}

function generateScanExecutionRulePreview(index, item) {
  //{key: key, type: "AAPolicy", dirty: policyarr[i].dirty, ruleId: [i], policy: [policyarr[i].policy]}
  policy = item.policy[0];
  $("#preview"+policytypestr+"rule").find(".previewbox").html("<span class=\"previewcontent\">"+policy.toRule()+"</span>");
}

function updateActions() {
  hasNetwork = false;
  hasFiles = false;
  hasPodStart = false;
  hasScanExecution = false;
  hasScanResult = false;
  hasPreventableAction = false;
  $("#"+policytypestr+" .rulesection").children()
    .each(function() {
      if ($(this).children(".ruletype")) {
        ruletype=$(this).children(".ruletype").children("button").html();
        switch(ruletype) {
          case "Network Traffic":
            hasNetwork = true;
            break;
          case "Files":
            hasFiles = true;
            break;
          case "A Pod attempts to start":
            hasPodStart = true;
            break;
          case "A pipeline is run":
          case "A commit is merged":
          case "Schedule":
            hasScanExecution = true;
            break;
          case "Any":
          case "SAST":
          case "Secret Detection":
          case "DAST":
          case "Dependency Scanning":
          case "Container Scanning":
          case "License Compliance":
          case "API Fuzzing":
          case "Coverage Fuzzing":
            hasScanResult = true;
            break;
        }
      }
    });
  $("#"+policytypestr+" .actionsection").children(".mandatory").remove();
  //add mandatory network action
  if (hasNetwork) {
    actionblock = $('<div class="actionblock networkaction mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    actionblock = dropdownSelector(actionblock,"actiontype",["allow"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "the network traffic.");
    actionblock = staticText(actionblock,"(Note: traffic that does not match any policy will be blocked)");
    $('<div class="clear"></div>').appendTo(actionblock);
  }
  if (hasFiles) {
    actionblock = $('<div class="actionblock filesaction mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    actionblock = dropdownSelector(actionblock,"actiontype",["allow","block"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "the file activity.");
    $('<div class="clear"></div>').appendTo(actionblock);
  }
  if (hasPodStart){
    actionblock = $('<div class="actionblock mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    actionblock = staticText(actionblock, "limit");
    actionblock = dropdownSelector(actionblock,"actiontypehidden",["System Configuration (sysctl)"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "System Configuration (sysctl)");
    actionblock = staticText(actionblock, "to allow the following unsafe sysctls ");
    actionblock = textboxInput(actionblock, "actionallowedunsafesysctls", "", APPENDTO);
    actionblock = staticText(actionblock, "and block setting ");
    actionblock = textboxInput(actionblock, "actionforbiddensysctls", "*", APPENDTO);
    actionblock = staticText(actionblock, "sysctls ");
    $('<div class="clear"></div>').appendTo(actionblock);
    
    actionblock = $('<div class="actionblock mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    actionblock = staticText(actionblock, "limit");
    actionblock = dropdownSelector(actionblock,"actiontypehidden",["Pod Capabilities"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "Pod Capabilities");
    actionblock = staticText(actionblock, "to add ");
    actionblock = multiselectSelector(actionblock,"actiondefaultaddcapabilities",capabilities,APPENDTO);
    actionblock = staticText(actionblock, "capabilities by default, optionally allow ");
    actionblock = multiselectSelector(actionblock,"actionallowedcapabilities",capabilities,APPENDTO);
    actionblock = staticText(actionblock, "capabilities, and remove ");
    actionblock = multiselectSelector(actionblock,"actionrequireddropcapabilities",capabilities,APPENDTO);
    actionblock = staticText(actionblock, "capabilities from all Linux pods");
    $('<div class="clear"></div>').appendTo(actionblock);

    actionblock = $('<div class="actionblock mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
    $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    actionblock = staticText(actionblock, "limit");
    actionblock = dropdownSelector(actionblock,"actiontypehidden",["Pod Privileges"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "Pod Privileges");
    //actionblock = dropdownSelector(actionblock,"actiontype",["User Privileges", "System Configuration (sysctl)", "Pod Capabilities"],"updatePodActionType(this);",false,APPENDTO);
    actionblock = staticText(actionblock, "to ");
    actionblock = dropdownSelector(actionblock,"actionuser",["any user", "any user except root", "specific users"],"updateUserOrGroup(this,'user');",false,APPENDTO);
    actionblock = staticText(actionblock, "and ");
    actionblock = dropdownSelector(actionblock,"actiongroup",["any group", "specific groups"],"updateUserOrGroup(this,'group');",false,APPENDTO);
    actionblock = staticText(actionblock, "and ");
    actionblock = dropdownSelector(actionblock,"actionprivescalation",["allow", "block"],null,false,APPENDTO);
    actionblock = staticText(actionblock, "privilege escalation ");
    $('<div class="clear"></div>').appendTo(actionblock);
  }
  if(hasScanExecution){
    //check if already exists.
    // if($("#"+policytypestr+" .actionsection .requirescanaction").length==0){
    //   actionblock = $('<div class="actionblock requirescanaction"></div>').prependTo("#"+policytypestr+" .actionsection");
    //   $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
    //   actionblock = dropdownSelector(actionblock,"actiontypehidden",["requirescan"],null,false,APPENDTO);
    //   actionblock = staticText(actionblock, "Require a");
    //   actionblock = dropdownSelector(actionblock,"actionscanner",["SAST","Secret Detection","DAST","Dependency Scanning","Container Scanning","License Compliance","API Fuzzing","Coverage Fuzzing"],"updateScanParameters(this,false);",false,APPENDTO);
    //   actionblock = staticText(actionblock, "scan to run");
    //   actionblock = dropdownSelector(actionblock,"actiontypehidden",["OTHER"],null,false,APPENDTO);
    //   $('<div class="clear"></div>').appendTo(actionblock);
    //   $('<i class="fas fa-trash deleteicon" onClick="deleteAction(this);"></i>').appendTo(actionblock);
    // }
  }
  if(hasScanResult){
    //check if already exists.  If exists, mark as mandatory.  If not, create it.
    if($("#"+policytypestr+" .actionsection .scanresultaction").length>0 && $("#"+policytypestr+" .actionsection .scanresultaction.mandatory").length==0){
      actionblock = $("#"+policytypestr+" .actionsection .scanresultaction").first();
      actionblock.addClass("mandatory");
    } else {
      actionblock = $('<div class="actionblock scanresultaction mandatory"></div>').prependTo("#"+policytypestr+" .actionsection");
      $('<span class="rulejoiner ruleelement">THEN&nbsp;</span>').appendTo(actionblock);
      actionblock = dropdownSelector(actionblock,"actiontype",["allow","fail"],null,false,APPENDTO);
      actionblock = staticText(actionblock, "the pipeline.");
      actionblock = staticText(actionblock,"(Note: this only applies if a pipeline triggered the scan)");
      $('<div class="clear"></div>').appendTo(actionblock);
    }
  }
  $("#"+policytypestr+" .actionsection")
    .children()
    .each(function(index) {
      if (index == 0) {
        $(this).children(".rulejoiner").html("THEN&nbsp;");
      } else {
        $(this).children(".rulejoiner").html("AND&nbsp;");
      }
    });
}

function colorize(text) {
  lines = text.split("\n");
  text = "";
  for (i = 0; i < lines.length; i++) {
    if (lines[i].indexOf(":") > -1) {
      text += '<span class="green">';
      text += lines[i].substring(0, lines[i].indexOf(":") + 1);
      text += "</span>";
      text += lines[i].substring(lines[i].indexOf(":") + 1, lines[i].length);
    } else if (lines[i].length > 0) {
      text += lines[i];
    }
    if (i + 1 < lines.length) {
      text += "\n";
    }
  }
  return text;
}

function findGetParameter(parameterName) {
  var result = null,
      tmp = [];
  var items = location.search.substr(1).split("&");
  for (var index = 0; index < items.length; index++) {
      tmp = items[index].split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  }
  return result;
}